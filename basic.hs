{-

  Cédric Villeneuve
  <vildric@gmail.com>
  http://vildric.no-ip.org:8050

    -}

module Basic
( head'
, tail'
, init'
, last'
, reverse'
, length'
, null'
, take'
, drop'
, minimum'
, maximum'
, sum'
, product'
, elem'
, cycle'
, repeat'
, replicate'
, zip'
, map'
, filter'
, palindrome
, compress
) where 

-- |
-- | Prelude functions
-- |

-- take a list and return the first element
head' :: [a] -> a
head' [] = error "head': empty list"
head' (x:xs) = x

-- take a list and return all but the first element
tail' :: [a] -> [a]
tail' [] = error "tail': empty list"
tail' (x:xs) = xs  

-- take a list and return all but the last element
init' :: [a] -> [a]
init' [x] = []
init' (x:xs) = x : init' xs

-- take a list and return the last element
last' :: [a] -> a 
last' [] = error "last': empty list"
last' (x:[]) = x
last' (x:xs) = last' xs

-- take a list and reverse it
reverse' :: [a] -> [a]
reverse' xs = foldl (\x y -> y : x) [] xs 

-- take a list and return the number of element that it have
length' :: [a] -> Int
length' [] = 0
length' (x:xs) = 1 + length' xs

length'' :: [a] -> Int
length'' xs = foldl(\x y -> x+1) 0 xs

-- check if a list is empty (null) or not
null' :: [a] -> Bool 
null' [] = True
null' _ = False

-- take `n` element of a list
take' :: Int -> [a] -> [a]
take' _ [] = [] 
take'  0 _ = []
take' a (x:xs) = x : take' (a - 1) xs

-- drop `n` element of a list
drop' :: Int -> [a] -> [a] 
drop' _ [] = []
drop' 0 xs = xs
drop' a (x:xs) = drop' (a - 1) xs

-- take a list and return the lower element
minimum' :: (Ord a) => [a] -> a
minimum' [] = error "minimum': empty list"
minimum' (x:[]) = x
minimum' (x:xs) = if x <= last' xs then minimum' $ x : init' xs
                  else minimum' xs

minimum'' :: (Ord a) => [a] -> a
minimum'' xs = foldl1 (\x y -> if x < y then x else y) xs 

-- take a list and return the higher element
maximum' :: (Ord a) => [a] -> a
maximum' [] = error "maximum': empty list"
maximum' (x:[]) = x
maximum' (x:xs) = if x >= last' xs then maximum' $ x : init' xs 
                  else maximum' xs

maximum'' :: (Ord a) => [a] -> a
maximum'' xs = foldl1 (\x y -> if x > y then x else y) xs

-- take a list and return the sum of is elements
sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

sum'' :: (Num a) => [a] -> a
sum'' xs = foldl (+) 0 xs

-- take a list and return the product of is elements
product' :: (Num a) => [a] -> a
product' [] = 0
product' (x:xs) = x * product' xs

product'' :: (Num a) => [a] -> a
product'' xs = foldl (*) 1 xs

-- take a list, a thing and check if this thing is in the list (True || False)
elem' :: (Eq a) =>  a -> [a] -> Bool
elem' _ [] = False
elem' a (x:[]) = (==) a x
elem' a (x:xs) = if (==) a x then True 
                 else elem' a xs

elem'' :: (Eq a) => a -> [a] -> Bool
elem'' x xs = foldl (\a b -> if x == b then True else a) False [] 

-- take a list and repeat it infinitely
cycle' :: [a] -> [a]
cycle' [] = error "cycle': empty list"
cycle' xs = xs ++ cycle' xs

-- take a thing and repeat it infinitely (in a list)
repeat' :: a -> [a]
repeat' a = a : repeat' a

-- take a thing and repeat it `n` time (in a list)
replicate' :: Int -> a -> [a]
replicate' 0 _ = []
replicate' a b = b : replicate' (a - 1) b

-- take two list and join it together in a list of tuple
zip' :: [a] -> [b] -> [(a,b)]
zip' [] _ = [] 
zip' _ [] = []
zip' (x:xs) (y:ys) = (x,y) : zip' xs ys 

-- take a function, a list and apply this function to each element of the list
map' :: (a -> a) -> [a] -> [a]
map' f [] = []
map' f (x:xs) = f x : map' f xs

-- take a function, a list and filter the list.
-- By example: filter' (== 2) [1,2,3] return [2]
filter' :: (a -> Bool) -> [a] -> [a]
filter' _ [] = []
filter' f (x:xs) = if not (f x) then filter' f xs
                   else x : filter' f xs
 
-- take a function, two list and apply this function on each element of 
-- two list. By example: zipWith' (+) [1,2] [3,4] return [4,6]
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c] 
zipWith' f [] _ = []
zipWith' f _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

-- take a function, two arguments and invert the arguments.
-- By example: flip' (++) "World!" "Hello, " return "Hello, World!"
flip' :: (a -> b -> c) -> b -> a -> c 
flip' f x y = f y x


-- |
-- | other functions
-- |

-- check if a list is a palindrome
palindrome :: (Eq a) => [a] -> Bool
palindrome [] = True
palindrome (x:[]) = True
palindrome (x:xs) = if (==) x (last' xs) then palindrome $ init' xs 
                    else False 

-- Eliminate consecutive duplicates of list elements 
compress :: (Eq a) => [a] -> [a]
compress [] = []
compress (x:[]) = [x]
compress (x:xs) = if x == head xs then compress xs 
                  else x : compress xs


